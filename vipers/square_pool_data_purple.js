{
	"localTeam": "McLean County Pony Express",
	"title": "2016 Super Bowl Square Pool",
	"topTeam": "Washington Sentinels",
	"sideTeam": "Miami Sharks",
	"topScores": [ "1", "8", "4", "0", "3", "7", "9", "2", "6", "5" ],
	"sideScores": [ "1", "2", "8", "9", "6", "0", "7", "3", "4", "5" ],
	"firstWinner": "Thad Craft",
	"secondWinner": "Cory Steers",
	"thirdWinner": "Jim Dandy",
	"fourthWinner": "Brent Calvert",
	"gridData": [
					[
						{
							"first": "Thad",
							"last": "Craft",
							"winner": "firstQuarter"
						},
						{
							"first": "Thad",
							"last": "Craft",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "Cory",
							"last": "Steers",
							"winner": "secondQuarter"
						}
					],
					[
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						}
					],
					[
						{
							"first": "Cory",
							"last": "Steers",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						}
					],
					[
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						}
					],
					[
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						}
					],
					[
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						}
					],
					[
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						}
					],
					[
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						}
					],
					[
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						}
					],
					[
						{
							"first": "Jim",
							"last": "Dandy",
							"winner": "thirdQuarter"
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "not",
							"last": "sold",
							"winner": ""
						},
						{
							"first": "Brent",
							"last": "Calvert",
							"winner": "fourthQuarter"
						}
					]
				]
}
