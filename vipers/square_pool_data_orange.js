{
	"localTeam": "McLean County Vipers",
	"title": "2016 Super Bowl Square Pool - Orange",
	"topTeam": "Carolina Panthers",
	"sideTeam": "Denver Broncos",
	"topScores": [ "3", "4", "0", "7", "5", "2", "9", "8", "6", "1" ],
	"sideScores": [ "7", "2", "8", "4", "1", "6", "9", "5", "0", "3" ],
	"firstWinner": "Doug Casey",
	"secondWinner": "Gary Calvert",
	"thirdWinner": "Tori Upton",
	"fourthWinner": "Doug Casey",
	"gridData": [
	[
		{
			"first": "Justin",
			"last": "Michels",
			"winner": ""

		},
		{
			"first": "Tim",
			"last": "Hezlep",
			"winner": ""

		},
		{
			"first": "Curt",
			"last": "Strubhar",
			"winner": ""

		},
		{
			"first": "Brian",
			"last": "Onsager",
			"winner": ""

		},
		{
			"first": "Bud",
			"last": "Tanton",
			"winner": ""

		},
		{
			"first": "Justin",
			"last": "Michels",
			"winner": ""

		},
		{
			"first": "Justin",
			"last": "Michels",
			"winner": ""

		},
		{
			"first": "Greg",
			"last": "Rueter",
			"winner": ""

		},
		{
			"first": "Doug",
			"last": "Casey",
			"winner": ""

		},
		{
			"first": "Chris",
			"last": "Arbuckle",
			"winner": ""

		}
	],
	[
		{
			"first": "?",
			"last": "Inch",
			"winner": ""

		},
		{
			"first": "Steve",
			"last": "Smith",
			"winner": ""

		},
		{
			"first": "Dave",
			"last": "Dykhuis",
			"winner": ""

		},
		{
			"first": "Jim",
			"last": "Bogle",
			"winner": ""

		},
		{
			"first": "Clint",
			"last": "Fernandez",
			"winner": ""

		},
		{
			"first": "Brenna",
			"last": "Whitehead",
			"winner": ""

		},
		{
			"first": "?",
			"last": "Inch",
			"winner": ""

		},
		{
			"first": "Tom",
			"last": "Fernandez",
			"winner": ""

		},
		{
			"first": "Larry",
			"last": "Phillips",
			"winner": ""

		},
		{
			"first": "Kara",
			"last": "Moline",
			"winner": ""

		}
	],
	[
		{
			"first": "Tori",
			"last": "Upton",
			"winner": ""

		},
		{
			"first": "Renea",
			"last": "Calvert",
			"winner": ""

		},
		{
			"first": "Jerd",
			"last": "Morstatter",
			"winner": ""

		},
		{
			"first": "Mike",
			"last": "Onsager",
			"winner": ""

		},
		{
			"first": "Tony",
			"last": "Morstatter",
			"winner": ""

		},
		{
			"first": "Ann",
			"last": "Kramer",
			"winner": ""

		},
		{
			"first": "Doug",
			"last": "Casey",
			"winner": ""

		},
		{
			"first": "Debbie",
			"last": "Lamb",
			"winner": ""

		},
		{
			"first": "Renea",
			"last": "Calvert",
			"winner": ""

		},
		{
			"first": "Jay",
			"last": "Pierce",
			"winner": ""

		}
	],
	[
		{
			"first": "Jenn",
			"last": "Mauer",
			"winner": ""

		},
		{
			"first": "Jim",
			"last": "Gordon",
			"winner": ""

		},
		{
			"first": "Doug",
			"last": "Casey",
			"winner": "fourthQuarter"

		},
		{
			"first": "Doug",
			"last": "Casey",
			"winner": ""

		},
		{
			"first": "Keegan",
			"last": "Buckner",
			"winner": ""

		},
		{
			"first": "Carrie",
			"last": "Delong",
			"winner": ""

		},
		{
			"first": "Pete",
			"last": "Odland",
			"winner": ""

		},
		{
			"first": "Paula",
			"last": "Morrill",
			"winner": ""

		},
		{
			"first": "Jim",
			"last": "Bogle",
			"winner": ""

		},
		{
			"first": "Jodi",
			"last": "Sellmeyer",
			"winner": ""

		}
	],
	[
		{
			"first": "Julie",
			"last": "Dunning",
			"winner": ""

		},
		{
			"first": "Brent",
			"last": "Giosta",
			"winner": ""

		},
		{
			"first": "Gary",
			"last": "Calvert",
			"winner": ""

		},
		{
			"first": "Jon",
			"last": "Whitehead",
			"winner": ""

		},
		{
			"first": "Thad",
			"last": "Craft",
			"winner": ""

		},
		{
			"first": "Paula",
			"last": "Morrill",
			"winner": ""

		},
		{
			"first": "Jerd",
			"last": "Morstatter",
			"winner": ""

		},
		{
			"first": "Jason",
			"last": "Steers",
			"winner": ""

		},
		{
			"first": "Robert",
			"last": "Steers",
			"winner": ""

		},
		{
			"first": "Robert",
			"last": "Steers",
			"winner": ""

		}
	],
	[
		{
			"first": "Gary",
			"last": "Pagano",
			"winner": ""

		},
		{
			"first": "Jenn",
			"last": "Mauer",
			"winner": ""

		},
		{
			"first": "Chris",
			"last": "Arbuckle",
			"winner": ""

		},
		{
			"first": "Tori",
			"last": "Upton",
			"winner": "thirdQuarter"

		},
		{
			"first": "Tim",
			"last": "Hezlep",
			"winner": ""

		},
		{
			"first": "Greg",
			"last": "Rueter",
			"winner": ""

		},
		{
			"first": "Paula",
			"last": "Morrill",
			"winner": ""

		},
		{
			"first": "Renea",
			"last": "Calvert",
			"winner": ""

		},
		{
			"first": "Andy",
			"last": "Meiners",
			"winner": ""

		},
		{
			"first": "Randy",
			"last": "Asper",
			"winner": ""

		}
	],
	[
		{
			"first": "Doug",
			"last": "Casey",
			"winner": ""

		},
		{
			"first": "Miranda",
			"last": "DeHaai",
			"winner": ""

		},
		{
			"first": "Lara",
			"last": "Thomas",
			"winner": ""

		},
		{
			"first": "Greg",
			"last": "Spillman",
			"winner": ""

		},
		{
			"first": "Justin",
			"last": "Michels",
			"winner": ""

		},
		{
			"first": "Justin",
			"last": "Michels",
			"winner": ""

		},
		{
			"first": "Justin",
			"last": "Michels",
			"winner": ""

		},
		{
			"first": "Mike",
			"last": "Dwinal",
			"winner": ""

		},
		{
			"first": "Gary",
			"last": "Pagano",
			"winner": ""

		},
		{
			"first": "Mary Beth",
			"last": "Wright",
			"winner": ""

		}
	],
	[
		{
			"first": "Tim",
			"last": "Morefield",
			"winner": ""

		},
		{
			"first": "Brian",
			"last": "Onsager",
			"winner": ""

		},
		{
			"first": "Darren",
			"last": "Plattner",
			"winner": ""

		},
		{
			"first": "Michelle",
			"last": "Gavvin",
			"winner": ""

		},
		{
			"first": "Brad",
			"last": "Gulley",
			"winner": ""

		},
		{
			"first": "Tim",
			"last": "Morefield",
			"winner": ""

		},
		{
			"first": "Reno",
			"last": "Giorgianni",
			"winner": ""

		},
		{
			"first": "BJ",
			"last": "Floyd",
			"winner": ""

		},
		{
			"first": "Chris",
			"last": "Arbuckle",
			"winner": ""

		},
		{
			"first": "Steve",
			"last": "Smith",
			"winner": ""

		}
	],
	[
		{
			"first": "Andy",
			"last": "Meiners",
			"winner": ""

		},
		{
			"first": "Bradyn",
			"last": "Whitehead",
			"winner": ""

		},
		{
			"first": "Doug",
			"last": "Casey",
			"winner": "firstQuarter"

		},
		{
			"first": "Greg",
			"last": "Spillman",
			"winner": ""

		},
		{
			"first": "Michelle",
			"last": "Whitehead",
			"winner": ""

		},
		{
			"first": "Justin",
			"last": "DeHaai",
			"winner": ""

		},
		{
			"first": "Brent",
			"last": "Giosta",
			"winner": ""

		},
		{
			"first": "Mike",
			"last": "Onsager",
			"winner": ""

		},
		{
			"first": "Krista",
			"last": "DeHaai",
			"winner": ""

		},
		{
			"first": "Anne",
			"last": "Robinson",
			"winner": ""

		}
	],
	[
		{
			"first": "FJ",
			"last": "Hafner",
			"winner": ""

		},
		{
			"first": "Darren",
			"last": "Plattner",
			"winner": ""

		},
		{
			"first": "Justin",
			"last": "Michels",
			"winner": ""

		},
		{
			"first": "Gary",
			"last": "Calvert",
			"winner": "secondQuarter"

		},
		{
			"first": "Jeremy",
			"last": "DeHaai",
			"winner": ""

		},
		{
			"first": "Doug",
			"last": "Casey",
			"winner": ""

		},
		{
			"first": "FJ",
			"last": "Hafner",
			"winner": ""

		},
		{
			"first": "Randy",
			"last": "Asper",
			"winner": ""

		},
		{
			"first": "Sydnie",
			"last": "DeHaai",
			"winner": ""

		},
		{
			"first": "Paula",
			"last": "Morrill",
			"winner": ""

		}
	]
]
}
